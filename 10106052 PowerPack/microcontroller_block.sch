EESchema Schematic File Version 5
EELAYER 33 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
$Comp
L Device:R R41
U 1 1 5E8821B6
P 2950 1800
F 0 "R41" V 2900 1600 50  0000 C CNN
F 1 "4.7k" V 2900 1950 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2880 1800 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/rchip/PYu-RC_Group_51_RoHS_L_10.pdf" H 2950 1800 50  0001 C CNN
F 4 "Yageo" V 2950 1800 50  0001 C CNN "Manufacturer"
F 5 "RC0603FR-074K7L" V 2950 1800 50  0001 C CNN "Manufacturer_No"
F 6 "1/10W" V 2950 1800 50  0001 C CNN "Power"
F 7 "1%" V 2950 1800 50  0001 C CNN "Tolerance"
	1    2950 1800
	-1   0    0    1   
$EndComp
$Comp
L Device:R R45
U 1 1 5E883018
P 2950 2950
F 0 "R45" V 2900 2750 50  0000 C CNN
F 1 "1k" V 2900 3100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2880 2950 50  0001 C CNN
F 3 "https://www.bourns.com/docs/Product-Datasheets/CHPREZTR.pdf" H 2950 2950 50  0001 C CNN
F 4 "Bourns" V 2950 2950 50  0001 C CNN "Manufacturer"
F 5 "CR0603-FX-1001ELF" V 2950 2950 50  0001 C CNN "Manufacturer_No"
F 6 "1/10W" V 2950 2950 50  0001 C CNN "Power"
F 7 "1%" V 2950 2950 50  0001 C CNN "Tolerance"
	1    2950 2950
	0    1    1    0   
$EndComp
$Comp
L Device:R R46
U 1 1 5E883403
P 2950 3050
F 0 "R46" V 2900 2850 50  0000 C CNN
F 1 "1k" V 2900 3200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2880 3050 50  0001 C CNN
F 3 "https://www.bourns.com/docs/Product-Datasheets/CHPREZTR.pdf" H 2950 3050 50  0001 C CNN
F 4 "Bourns" V 2950 3050 50  0001 C CNN "Manufacturer"
F 5 "CR0603-FX-1001ELF" V 2950 3050 50  0001 C CNN "Manufacturer_No"
F 6 "1/10W" V 2950 3050 50  0001 C CNN "Power"
F 7 "1%" V 2950 3050 50  0001 C CNN "Tolerance"
	1    2950 3050
	0    1    1    0   
$EndComp
$Comp
L Device:R R43
U 1 1 5E882ABA
P 6150 2150
F 0 "R43" V 6100 1950 50  0000 C CNN
F 1 "1k" V 6100 2300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6080 2150 50  0001 C CNN
F 3 "https://www.bourns.com/docs/Product-Datasheets/CHPREZTR.pdf" H 6150 2150 50  0001 C CNN
F 4 "Bourns" V 6150 2150 50  0001 C CNN "Manufacturer"
F 5 "CR0603-FX-1001ELF" V 6150 2150 50  0001 C CNN "Manufacturer_No"
F 6 "1/10W" V 6150 2150 50  0001 C CNN "Power"
F 7 "1%" V 6150 2150 50  0001 C CNN "Tolerance"
	1    6150 2150
	0    1    1    0   
$EndComp
$Comp
L Device:R R42
U 1 1 5E882774
P 6150 2250
F 0 "R42" V 6100 2050 50  0000 C CNN
F 1 "1k" V 6100 2400 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6080 2250 50  0001 C CNN
F 3 "https://www.bourns.com/docs/Product-Datasheets/CHPREZTR.pdf" H 6150 2250 50  0001 C CNN
F 4 "Bourns" V 6150 2250 50  0001 C CNN "Manufacturer"
F 5 "CR0603-FX-1001ELF" V 6150 2250 50  0001 C CNN "Manufacturer_No"
F 6 "1/10W" V 6150 2250 50  0001 C CNN "Power"
F 7 "1%" V 6150 2250 50  0001 C CNN "Tolerance"
	1    6150 2250
	0    1    1    0   
$EndComp
$Comp
L Device:R R40
U 1 1 5E881E70
P 6150 2350
F 0 "R40" V 6100 2150 50  0000 C CNN
F 1 "1k" V 6100 2500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6080 2350 50  0001 C CNN
F 3 "https://www.bourns.com/docs/Product-Datasheets/CHPREZTR.pdf" H 6150 2350 50  0001 C CNN
F 4 "Bourns" V 6150 2350 50  0001 C CNN "Manufacturer"
F 5 "CR0603-FX-1001ELF" V 6150 2350 50  0001 C CNN "Manufacturer_No"
F 6 "1/10W" V 6150 2350 50  0001 C CNN "Power"
F 7 "1%" V 6150 2350 50  0001 C CNN "Tolerance"
	1    6150 2350
	0    1    1    0   
$EndComp
$Comp
L Device:R R38
U 1 1 5E881706
P 6150 2450
F 0 "R38" V 6100 2250 50  0000 C CNN
F 1 "1k" V 6100 2600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6080 2450 50  0001 C CNN
F 3 "https://www.bourns.com/docs/Product-Datasheets/CHPREZTR.pdf" H 6150 2450 50  0001 C CNN
F 4 "Bourns" V 6150 2450 50  0001 C CNN "Manufacturer"
F 5 "CR0603-FX-1001ELF" V 6150 2450 50  0001 C CNN "Manufacturer_No"
F 6 "1/10W" V 6150 2450 50  0001 C CNN "Power"
F 7 "1%" V 6150 2450 50  0001 C CNN "Tolerance"
	1    6150 2450
	0    1    1    0   
$EndComp
$Comp
L Device:R R34
U 1 1 5E880747
P 6150 2550
F 0 "R34" V 6100 2350 50  0000 C CNN
F 1 "1k" V 6100 2700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6080 2550 50  0001 C CNN
F 3 "https://www.bourns.com/docs/Product-Datasheets/CHPREZTR.pdf" H 6150 2550 50  0001 C CNN
F 4 "Bourns" V 6150 2550 50  0001 C CNN "Manufacturer"
F 5 "CR0603-FX-1001ELF" V 6150 2550 50  0001 C CNN "Manufacturer_No"
F 6 "1/10W" V 6150 2550 50  0001 C CNN "Power"
F 7 "1%" V 6150 2550 50  0001 C CNN "Tolerance"
	1    6150 2550
	0    1    1    0   
$EndComp
$Comp
L Device:R R33
U 1 1 5E87F0BB
P 6150 2650
F 0 "R33" V 6100 2450 50  0000 C CNN
F 1 "1k" V 6100 2800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6080 2650 50  0001 C CNN
F 3 "https://www.bourns.com/docs/Product-Datasheets/CHPREZTR.pdf" H 6150 2650 50  0001 C CNN
F 4 "Bourns" V 6150 2650 50  0001 C CNN "Manufacturer"
F 5 "CR0603-FX-1001ELF" V 6150 2650 50  0001 C CNN "Manufacturer_No"
F 6 "1/10W" V 6150 2650 50  0001 C CNN "Power"
F 7 "1%" V 6150 2650 50  0001 C CNN "Tolerance"
	1    6150 2650
	0    1    1    0   
$EndComp
$Comp
L Device:R R37
U 1 1 5E8813E2
P 6150 3050
F 0 "R37" V 6100 2850 50  0000 C CNN
F 1 "1k" V 6100 3200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6080 3050 50  0001 C CNN
F 3 "https://www.bourns.com/docs/Product-Datasheets/CHPREZTR.pdf" H 6150 3050 50  0001 C CNN
F 4 "Bourns" V 6150 3050 50  0001 C CNN "Manufacturer"
F 5 "CR0603-FX-1001ELF" V 6150 3050 50  0001 C CNN "Manufacturer_No"
F 6 "1/10W" V 6150 3050 50  0001 C CNN "Power"
F 7 "1%" V 6150 3050 50  0001 C CNN "Tolerance"
	1    6150 3050
	0    1    1    0   
$EndComp
$Comp
L Device:R R39
U 1 1 5E881B4F
P 6150 3150
F 0 "R39" V 6100 2950 50  0000 C CNN
F 1 "1k" V 6100 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6080 3150 50  0001 C CNN
F 3 "https://www.bourns.com/docs/Product-Datasheets/CHPREZTR.pdf" H 6150 3150 50  0001 C CNN
F 4 "Bourns" V 6150 3150 50  0001 C CNN "Manufacturer"
F 5 "CR0603-FX-1001ELF" V 6150 3150 50  0001 C CNN "Manufacturer_No"
F 6 "1/10W" V 6150 3150 50  0001 C CNN "Power"
F 7 "1%" V 6150 3150 50  0001 C CNN "Tolerance"
	1    6150 3150
	0    1    1    0   
$EndComp
$Comp
L Device:R R36
U 1 1 5E880FA4
P 6150 3250
F 0 "R36" V 6100 3050 50  0000 C CNN
F 1 "1k" V 6100 3400 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6080 3250 50  0001 C CNN
F 3 "https://www.bourns.com/docs/Product-Datasheets/CHPREZTR.pdf" H 6150 3250 50  0001 C CNN
F 4 "Bourns" V 6150 3250 50  0001 C CNN "Manufacturer"
F 5 "CR0603-FX-1001ELF" V 6150 3250 50  0001 C CNN "Manufacturer_No"
F 6 "1/10W" V 6150 3250 50  0001 C CNN "Power"
F 7 "1%" V 6150 3250 50  0001 C CNN "Tolerance"
	1    6150 3250
	0    1    1    0   
$EndComp
$Comp
L Device:R R35
U 1 1 5E880BBF
P 6150 3350
F 0 "R35" V 6100 3150 50  0000 C CNN
F 1 "1k" V 6100 3500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6080 3350 50  0001 C CNN
F 3 "https://www.bourns.com/docs/Product-Datasheets/CHPREZTR.pdf" H 6150 3350 50  0001 C CNN
F 4 "Bourns" V 6150 3350 50  0001 C CNN "Manufacturer"
F 5 "CR0603-FX-1001ELF" V 6150 3350 50  0001 C CNN "Manufacturer_No"
F 6 "1/10W" V 6150 3350 50  0001 C CNN "Power"
F 7 "1%" V 6150 3350 50  0001 C CNN "Tolerance"
	1    6150 3350
	0    1    1    0   
$EndComp
$Comp
L Device:R R47
U 1 1 5E88371E
P 6150 4150
F 0 "R47" V 6100 3950 50  0000 C CNN
F 1 "1k" V 6100 4300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6080 4150 50  0001 C CNN
F 3 "https://www.bourns.com/docs/Product-Datasheets/CHPREZTR.pdf" H 6150 4150 50  0001 C CNN
F 4 "Bourns" V 6150 4150 50  0001 C CNN "Manufacturer"
F 5 "CR0603-FX-1001ELF" V 6150 4150 50  0001 C CNN "Manufacturer_No"
F 6 "1/10W" V 6150 4150 50  0001 C CNN "Power"
F 7 "1%" V 6150 4150 50  0001 C CNN "Tolerance"
	1    6150 4150
	0    1    1    0   
$EndComp
$Comp
L Device:R R44
U 1 1 5E882D06
P 8050 3750
F 0 "R44" V 8000 3550 50  0000 C CNN
F 1 "4.7k" V 8000 3950 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 7980 3750 50  0001 C CNN
F 3 "http://www.vishay.com/docs/20035/dcrcwe3.pdf" H 8050 3750 50  0001 C CNN
F 4 "Vishay" V 8050 3750 50  0001 C CNN "Manufacturer"
F 5 "CRCW12064K70JNEA" V 8050 3750 50  0001 C CNN "Manufacturer_No"
F 6 "1/4W" V 8050 3750 50  0001 C CNN "Power"
F 7 "5%" V 8050 3750 50  0001 C CNN "Tolerance"
	1    8050 3750
	0    1    1    0   
$EndComp
$Comp
L 10106052_PowerPack:PIC18F43K22T-IPT U4
U 1 1 5E87E2C8
P 4550 3150
F 0 "U4" H 4550 4631 50  0000 C CNN
F 1 "PIC18F43K22T-IPT" H 4550 4540 50  0000 C CNN
F 2 "Package_QFP:TQFP-44_10x10mm_P0.8mm" H 4550 3150 50  0001 C CIN
F 3 "" H 4550 2900 50  0001 C CNN
	1    4550 3150
	1    0    0    -1  
$EndComp
$EndSCHEMATC
